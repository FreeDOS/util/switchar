;                                                                           ;
;                      SWITCHAR.S  v1.00a  08-13-1998                       ;
;                       Copyright 1998,  Charles Dye                        ;
;                       email:  raster@highfiber.com                        ;
;                                                                           ;
;       This is source for Eric Isaacson's shareware assembler, A86.        ;
;       Re-assemble by typing A86 SWITCHAR.S                                ;
;                                                                           ;
;       This program is copyrighted, but may be freely distributed          ;
;       under the terms of the Free Software Foundation's GNU general       ;
;       public license v2 (or later.)  See the file COPYING for the         ;
;       legalities.  If you did not receive a copy of COPYING, you          ;
;       may request one from the Free Software Foundation, Inc.,            ;
;       59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.           ;
;       ABSOLUTELY NO WARRANTY -- use it at your own risk!                  ;
;                                                                           ;

radix 16                               ; gentlemen prefer hex

doscall macro                          ; call to dos int_21 with a
mov ah,#1                              ; one-byte function in ah
int 21
#em

doscall2 macro                         ; call to dos int_21 with a
mov ax,#1                              ; two-byte function in ax
int 21
#em

dosprint macro                         ; call to dos string-print function
mov dx,offset #1
mov ah,09
int 21
#em


temp          equ  005c                ; temporary variable
old_strategy  equ  005e                ; original memory-allocation strategy
old_umb_link  equ  005f                ; original umb link state


begin:
call check_dos                         ; verify dos version is 2 or better
call release_memory                    ; release memory not needed by program
call parse_line                        ; parse the command line
call test_support                      ; decide whether switchar is supported
call install_handler                   ; install new int21 handler if needed
call set_user_switchar                 ; change switchar if desired
call show_current_switchar             ; display current setting
mov al,b [temp]                        ; get return code
doscall 4c                             ; and exit


check_dos:                             ; confirm dos is not precambrian :
cld
doscall2 3000                          ; get dos version number
cmp al,02                              ; two or better
jae ret                                ; is okay
dosprint msg_err_bad_dos               ; otherwise, print an error message
int 20                                 ; and kill program in an antique way
ret


release_memory:                        ; deallocate unneeded memory :
pop w [temp]                           ; stash subroutine call return address
mov ax,sp                              ; examine stack pointer:
cmp ax,stack_end                       ; plenty of room?
jae > l1
trs_80:                                ; not enough memory:
dosprint msg_trs_80                    ; complain
doscall2 4c11                          ; and exit with errorlevel 11
l1:
mov sp,stack_end                       ; reduce stack size
mov ax,0000                            ; push zero word onto stack
push ax                                ; for possible exit via ret
push w [temp]                          ; restore return address to stack
push cs
pop es                                 ; get program segment into .es
mov bx,(stack_end / 10) + 1
doscall 4a                             ; and shrink program's memory block
jc trs_80                              ; any error, assume not enough memory
ret


parse_line:                            ; parse through command line :
mov si,0081
parseloop:                             ; main loop
lodsb                                  ; get byte from command line
call test_eol                          ; found end of command line?
je ret                                 ; if so, done parsing
call test_space                        ; found a space or tab?
je parseloop                           ; if so, ignore it
call test_sw                           ; found a slash or minus?
je set_new_sc                          ; if so, use it as the new switchar
call force_uc                          ; force letter to uppercase
cmp al,'L'                             ; found option l ?
je parse_sw_l                          ; if so, go handle it
cmp al,'U'                             ; found option u ?
je parse_sw_u                          ; if so, go handle it
jmp syntax                             ; anything else is a syntax error

set_new_sc:                            ; found a switch character :
cmp al,b [new_sc]                      ; matches earlier ones?
je parseloop                           ; yes, just ignore it
cmp b [new_sc],00                      ; is this the first switchar found?
jne > l10                              ; no, problem ... go complain about it
mov b [new_sc],al                      ; otherwise, remember new switchar
jmp parseloop                          ; and continue parsing
l10:                                   ; different switch characters found :
dosprint msg_err_confused              ; complain
mov b [new_sc],00                      ; don't use either one
ret                                    ; and quit parsing

parse_sw_l:                            ; found option l :
or b [flags],08                        ; set no-upper-memory flag
jmp parseloop                          ; and continue parsing

parse_sw_u:                            ; found option u :
or b [flags],04                        ; set uninstall flag
jmp parseloop                          ; and continue parsing


test_support:                          ; is switchar api support present?
doscall2 3700                          ; get current switchar
cmp al,00                              ; success reported?
jne ret                                ; if not, assume failure
cmp dl,20                              ; impossible value for switchar?
jb ret                                 ; if so, fail (dos-c does this)
mov b [temp],dl                        ; save reported character
mov dl,'-'
doscall2 3701                          ; try setting switchar to '-'
cmp al,01                              ; .al set to anything but 0 or 1?
ja ret                                 ; if so, fail
mov dl,0ff
doscall2 3700                          ; get current switchar
cmp dl,'-'                             ; correctly set to '-' ?
jne ret                                ; if not, failure
mov dl,'/'
doscall2 3701                          ; try setting switchar to '/'
cmp al,01                              ; .al set to anything but 0 or 1?
ja ret                                 ; if so, fail
mov dl,0ff
doscall2 3700                          ; get current switchar
cmp dl,'/'                             ; correctly set to '/' ?
jne ret                                ; if not, failure
or b [flags],80                        ; set flag to note support present
mov dl,b [temp]
doscall2 3701                          ; restore the original value
ret                                    ; and exit

install_handler:                       ; install api support if needed :
test b [flags],04                      ; option u for uninstall?
if ne jmp remove_handler               ; if so, remove it instead
test b [flags],80                      ; support already present?
jne ret                                ; if so, just exit
doscall2 5800                          ; get current allocation strategy
if c jmp install_fail_3                ; deal with possible problems
mov b [old_strategy],al                ; save original strategy
test b [flags],08                      ; allowed to use umbs?
jne try_conventional                   ; if not, skip over umb attempt
doscall2 5802                          ; get current umb link state
mov b [old_umb_link],al                ; and save it
jc try_conventional                    ; if error, skip over umb attempt
mov bx,0001
doscall2 5803                          ; try to link in upper memory
jc try_conventional                    ; if that doesn't work, try low memory
mov bx,0041
doscall2 5801                          ; set strategy to best-fit-high-only
jc try_conventional                    ; if that doesn't work, try low memory
mov bx,resident_size
doscall 48                             ; attempt to allocate memory
mov w [temp],ax                        ; save segment (or error code)
pushf                                  ; and flags
mov bl,b [old_umb_link]
mov bh,00
doscall2 5803                          ; restore original umb link state
popf                                   ; did the allocation succeed?
jnc got_memory                         ; if so, go use the memory
try_conventional:                      ; can't use umb for int21 handler :
mov bx,0002
doscall2 5801                          ; set strategy to last-fit-low
if c jmp install_fail_2                ; deal with possible problems
mov bx,resident_size
doscall 48                             ; attempt to allocate memory
if c jmp install_fail_2                ; deal with possible problems
mov w [temp],ax                        ; success!  save segment
got_memory:                            ; successfully allocated memory :
mov bl,b [old_strategy]
mov bh,00
doscall2 5801                          ; restore original allocation strategy
mov es,w [temp]                        ; point .es to resident segment
mov si,resident_bytes                  ; and get number of bytes to copy
l10:
dec si
mov al,b [resident+si]                 ; copy byte of resident code
mov b [es:si],al                       ; into its new home
cmp si,0000
jne l10                                ; continue until done
doscall2 3521                          ; get int21 vector
mov ax,es                              ; in ax:bx
mov es,w [temp]
mov w [es:old21off],bx                 ; poke in offset of  old int21 vector
mov w [es:old21seg],ax                 ; poke in segment of old int21 vector
mov ax,es
dec ax
mov es,ax                              ; point to segment's arena header
inc ax
mov bl,b [es:0000]                     ; and examine the first byte
cmp bl,'M'                             ; if it's an 'm' continue
je > l30
cmp bl,'Z'                             ; if it's a  'z' continue
jne install_fail_3                     ; anything else, bomb out immediately
l30:
push cs
pop bx
cmp w [es:0001],bx                     ; make sure the owner word is
jne install_fail_3                     ; bomb out if it ain't
cmp w [es:0003],resident_size          ; make sure the size word is
jne install_fail_3                     ; bomb out if it ain't
mov w [es:0001],ax                     ; fuck with the arena owner word
mov si,0008
l40:
dec si
mov al,b [arena_sig+si]                ; copy a byte from the signature
mov b [es:0008+si],al                  ; into the arena header
cmp si,0000
jne l40                                ; until done
mov dx,new21off
mov ds,w [temp]
doscall2 2521                          ; point dos int21 to the new handler
push cs
pop ds                                 ; fix .ds
dosprint msg_api_installed             ; print 'installed' message
mov cx,w [temp]
call hexout4                           ; and the segment address
ret                                    ; and exit

install_fail_2:
mov bl,b [old_strategy]
mov bh,00
doscall2 5801                          ; restore original allocation strategy
install_fail_3:
dosprint msg_err_install
ret


remove_handler:
doscall2 3521
mov w [temp],es
cmp bx,new21off
jne remove_fail
mov si,offset old21off
l10:
dec si
mov al,b [resident+si]
cmp al,b [es:si]
jne remove_fail
cmp si,0000
jne l10
mov dx,w [es:old21off]
mov ds,w [es:old21seg]
doscall2 2521
push cs
pop ds
doscall 49
dosprint msg_removed
ret

remove_fail:
dosprint msg_err_remove
ret


set_user_switchar:                     ; change switchar if desired :
cmp b [new_sc],00
je ret
mov dl,b [new_sc]
doscall2 3701
ret


show_current_switchar:                 ; display current value of switchar :
doscall2 3700                          ; get current switchar from dos
mov b [msg_switchar_2],dl              ; and poke it into the message string
cmp dl,'/'
jne > l10                              ; if current switchar is '/'
mov dl,00                              ; return code is 0
l10:                                   ; otherwise, return code is the
mov b [temp],dl                        ; ascii value of the current switchar
dosprint msg_switchar_is               ; display message string
ret                                    ; and exit


syntax:
dosprint msg_syntax
doscall2 4c10


test_eol:
cmp al,00
je ret
cmp al,0d
ret

test_space:
cmp al,20
je ret
cmp al,09
ret

test_sw:
push si
mov si,0ffff
l10:
inc si
cmp b [switch_list+si],00
je > l20
cmp al,b [switch_list+si]
jne l10
pop si
ret
l20:
pop si
cmp al,00
ret

force_uc:
cmp al,'a'
jb ret
cmp al,'z'
ja ret
and al,0df
ret

hexout4:
mov dh,ch
mov ch,cl                              ; 8088-compatible way to do ror cx,08
mov cl,dh
call hexout2
mov dh,ch
mov ch,cl                              ; 8088-compatible way to do ror cx,08
mov cl,dh
hexout2:
mov dl,cl
shr dl,01
shr dl,01
shr dl,01                              ; 8088-compatible way to do shr dl,04
shr dl,01
call hexout1
mov dl,cl
and dl,0f
hexout1:
cmp dl,0a
jb > l1
add dl,37
jmp > l2
l1:
add dl,'0'
l2:
mov ah,02
int 21
ret

; ===========================================================================

resident:

new_int21:                             ; new front end for int-21 handler :
cmp ax,3700                            ; call to get-switchar function?
je new_get_switchar                    ; if so, jump to replacement handler
cmp ax,3701                            ; call to set-switchar function?
je new_set_switchar                    ; if so, jump to replacement handler
go_old21:                              ; otherwise jump to old int 21 handler
jmp 0ffff:0ffff                        ; (address will be poked in later)

new_get_switchar:                      ; replacement get-switchar function :
mov dl,b [cs:(my_sc - resident)]       ; load .dl from variable
mov al,00                              ; zero .al to indicate success
iret                                   ; and exit

new_set_switchar:                      ; replacement set-switchar function :
mov b [cs:(my_sc - resident)],dl       ; set variable from .dl
iret                                   ; and exit

my_sc:                                 ; replacement switchar variable :
db '/'

dw 0000                                ; needed to fool mem.exe 6.22 -- why?


new21off        equ  new_int21 - resident
old21off        equ  go_old21 - resident + 1
old21seg        equ  go_old21 - resident + 3

resident_bytes  equ  $ - resident
resident_size   equ  (resident_bytes + 0f) / 10

; ===========================================================================

new_sc:                                ; new value for switchar,
db 00                                  ; or 00 for no change

flags:                                 ; bit 7 = switchar api support present
db 00                                  ; bit 3 = do not use umb's           L
                                       ; bit 2 = remove / do not install    U



msg_markver:                           ; for mark aitchison's version utility
db 'VeRsIoN=1.00a',00
db 'CoPyRiGhT=Copyright 1998, Charles Dye',00

arena_sig:                             ; signature poked into arena header
db 'SwitChar'                          ; of resident code

switch_list:                           ; null-terminated list of valid
db '/-`~',00                           ; switch characters


msg_syntax:
db 0d,0a
db 'SWITCHAR.COM   v1.00a   08-13-1998   C. Dye   raster@highfiber.com',0d,0a
db 'Freeware.  Copyright 1998, Charles Dye.   No warranty!',0d,0a
db 0a
db 'Get/set current switchar.',0d,0a
db 0a
db 'SWITCHAR     display the current switchar',0d,0a
db 'SWITCHAR /   use slashes (DOS default)',0d,0a
db 'SWITCHAR -   use dashes  (UNIX style)',0d,0a
db 0a
db 'If your DOS does not support the switchar API, a small bit of resident',0d,0a
db 'code will be installed to handle it correctly.  SWITCHAR U will remove',0d,0a
db 'this resident code, if nothing else has hooked DOS since then.',0d,0a
db 0a
db 'This program may be freely distributed under the terms of the Free',0d,0a
db 'Software Foundation''s GNU General Public License, version 2; or, at',0d,0a
db 'your option, any later version of that License.  See the file COPYING',0d,0a
db 'for details.',0d,0a
db '$'

msg_err_bad_dos:
db 0d,0a,'Error:  DOS 2 or better required!',0d,0a,'$'

msg_trs_80:
db 0d,0a,'Error:  Not enough memory!',0d,0a,'$'

msg_err_install:
db 0d,0a,'Unable to install resident code!',0d,0a,'$'

msg_err_remove:
db 0d,0a,'Resident code not present, or can''t be removed!$'

msg_api_installed:
db 0d,0a,'Resident code installed in segment $'

msg_removed:
db 0d,0a,'Resident code removed.$'

msg_err_confused:
db 0d,0a,'Quit trying to confuse me.$'

msg_switchar_is:
db 0d,0a,'Switchar is "'
msg_switchar_2:
db '#"',0d,0a,'$'

stack_start  equ  ($ + 0f) and 0fff0   ; stack starts on paragraph
stack_end    equ  stack_start + 07fe   ; stack is about 2k long


;
;  HISTORY:
;
;  1.00   04-10-1998   1821 bytes
;         First release.
;
;  1.00a  08-13-1998   1834 bytes
;         Fixes an incompatibility with 8086 and 8088 CPUs (logical shifts
;  with immediate count values.)
